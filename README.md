
## POC: Automating the CI/CD process on AWS

## Purpose
The purpose of this task is to automate the deployment of the AWS infrastructure and deploy the CI/CD and configure the CI/CD and deploy the sample application

## Version History

```
Version     Date            Remarks(s)
0.1         31-10-2018      Initial version.

```

## Prerequisites

To be able to execute the playbook/role the following requirements need to be in place:

- AWS account 
- Ansible server deployed
- source code (Bitbucket)
- Requires Ansible 2.4 or newer

## Procedure

The entire flow is been setup in three parts:

1] Setting up the AWS infrastructure and deploying two EC2 instances for the same.

The role `aws-infra` is called to setup the aws infrastructure.The role performs the following task
  a) Creation of VPC
  b) Creation of Subnet 
  c) Creation of InternetGateway
  d) Creation of Route table
  e) Creation of Security group
  f) Creation of keypairs
  g) Creation of EC2 (Jenkins and Nodejs machine)

 The way to execute the role is by calling the playbook `infra_setup.yml`
```
 ansible-playbook infra-setup.yml 
```

As the infra creation is success it creates two EC2 server 1. Jenkins Server 2. Nodejs-App Server

The two inventory files are created after the execution of the above playbook 

The name of Inventory file is 

`jenkins_inventory` and 
`linux_npm_inventory`

2] Setting up the jenkins Machine and Configuring the jenkins and executing the Jobs


The following roles are called to install the jenkins and configure the jenkins.

roles: 
  - jenkins_install

  This role installs the jenkins on Rhel/Centos Server

   - jenkins_plugins

   This role installs the required plugins , the list of array of the plugins has to passed in the variable.

   - slave-agent

   This role configure the slave node wiht jenkins so that the slave acts as a development environment , where the  development will be executed by jenkins job

   `note: the above slave-agent role should be executed after the nodejs_task.yml playbook is executed.

   - jenkins_job_create

This role configure  the jenkins job with the bitbucket repo:

 The jenkins job will be executed for the evry commit thats made in the repository or every 6 hours


    - git_webhook

 This role is used to configure the webhook on bitbucket  of jenkins, so that jenkins can do the build when the commit is made.   


To install  configure and execute the jenkins related tasks:

```
ansible-playbook -i jenkins_inventory jenkins_tasks.yml
```

3] Installing and configuring the nodejs  server. The code from the bitbucket repo will be built on this environment by the jenkins.

  - nodejs_install

  The above role is used to configure the machine and install the nodejs and git packages.

  so that the commands like npm install and npm start can be executed.


  The following can be executed using the following command

```
 ansible-playbook -i linux_npm_inventory nodejs-task.yml

```

## Security Implemented:

AWS infrastructure is setup, so the setup is installed in our VPC and the firewall is applied using the security groups by limiting the ports and accessability.

Using the private keys for the authentications with the servers.(no username and passwords)

All the variables and credentials are encrypted using the Ansible Vault Encryption feature.(AES 256 Encryption)

## Containerization Approach for Source code Builds:

As the Containerization is the modernization techniques used for deploying the applications , we can even build the docker images and publish it back to the docker repository using Jenkins.

The Jenkins fetches the source code from the Repo and it reads the jenkinsfile to set up Jenkins to build and publish the image automatically, whenever you commit changes to your code repository.









## Acknowledgments

I would like to thank my colleagues for always helping me out.